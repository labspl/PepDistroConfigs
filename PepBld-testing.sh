#!/bin/bash
PATH="/sbin:/usr/sbin:/usr/local/sbin:$PATH"
#exec 3>&1 4>&2
#trap 'exec 2>&4 1>&3' 0 1 2 3
#exec 1>log.out 2>&1
# Remove fusato/*
# Incase of a failed build, resulting in stale mounts, add error handling after `lb build` exits.
[ ! -d fusato ] && rm -f fusato && mkdir fusato || mkdir fusato
#rm -rf  fusato/*
#rm -rf  fusato/.build

# Set the working folder variable
uchinanchu="$(pwd)"

# Move to the build folder
cd fusato
lb clean

# Set of the structure to be used for the ISO and Live system.
# See /usr/lib/live/build/config for a full list of examples.
# Up above is the manual description of what options I used so far.
lb config \
--color \
--quiet \
--archive-areas "main contrib non-free" \
--architectures amd64 \
--apt-recommends true \
--binary-images iso-hybrid \
--cache true \
--mode debian \
--distribution bullseye \
--firmware-binary true \
--firmware-chroot true \
--iso-application "PeppermintOS" \
--iso-preparer "PeppermintOS-https://peppermintos.com/" \
--iso-publisher "Peppermint OS Team" \
--iso-volume "PeppermintOS" \
--image-name "PepOS" \
--linux-flavours amd64 \
--security false \
--updates true \
--win32-loader false \
--checksums sha512 \
--zsync false \


# Install the XFCE Desktop 
mkdir -p     $uchinanchu/fusato/config/package-lists/
echo xfce4 > $uchinanchu/fusato/config/package-lists/desktop.list.chroot 

# Install software
echo "# Install software to the squashfs for calamares to unpack to the OS.
arandr 
arc-theme 
calamares-settings-debian 
calamares 
curl 
cups 
dconf-editor 
dialog 
dkms 
dbus-x11 
firmware-linux-nonfree 
firmware-linux 
firmware-misc-nonfree 
firmware-realtek 
firmware-atheros 
firmware-bnx2 
firmware-bnx2x 
firmware-brcm80211 
firmware-intelwimax 
firmware-libertas 
firmware-netxen 
firmware-zd1211 
fonts-cantarell 
gparted 
gnome-disk-utility 
gnome-system-tools 
grub-common 
grub2-common 
gvfs-backends 
gir1.2-webkit2-4.0 
inxi
locales-all
menulibre 
nemo 
neofetch 
network-manager-gnome 
python3-pip 
python3-tk 
python3-bs4 
python3-requests 
python3-ttkthemes 
python3-pyqt5.qtsvg
python3-pyqt5.qtwebkit
python3-apt
samba 
screenfetch 
smartmontools 
sqlite3 
synaptic 
system-config-printer 
mousepad 
xfce4-battery-plugin 
xfce4-clipman-plugin 
xfce4-power-manager 
xfce4-taskmanager 
xfce4-terminal 
xfce4-screenshooter 
xfce4-whiskermenu-plugin 
yad 
wget 
" > $uchinanchu/fusato/config/package-lists/packages.list.chroot 


# Get packages used ONLY by the live-session, to be stored in /pool by `lb`.
echo "# Packages required only by the live session.

" > $uchinanchu/fusato/config/package-lists/installer.list.binary 


# Setup the chroot structure
mkdir -p $uchinanchu/fusato/config/includes.binary
mkdir -p $uchinanchu/fusato/config/includes.bootstrap/etc/apt
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/applications
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/backgrounds
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/desktop-base
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/icons/default
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/themes
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/peppermint
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/pixmaps
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/plymouth
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/ice/locale
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/xfce4/helpers
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/polkit-1/actions
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/fonts/pepconf
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/bin
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/sbin
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/lib
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/lib/peppermint/ice
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/apt
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/apt/preferences.d
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/calamares
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/default
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/lightdm
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/live/config.conf.d
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/skel/.config/autostart
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/skel/.local/share
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/xdg/xfce4
mkdir -p $uchinanchu/fusato/config/includes.chroot/opt
mkdir -p $uchinanchu/fusato/config/includes.chroot/opt/pypep
mkdir -p $uchinanchu/fusato/config/includes.chroot/opt/iceinst
mkdir -p $uchinanchu/fusato/config/includes.chroot/opt/startpep
mkdir -p $uchinanchu/fusato/config/includes.chroot/opt/pepconf

mkdir -p $uchinanchu/fusato/config/hooks/normal

# Copy Configs to the chroot
cp $uchinanchu/pepcal/adddesktopicon/add-calamares-desktop-icon $uchinanchu/fusato/config/includes.chroot/usr/bin
cp $uchinanchu/pepcal/calamares/settings.conf $uchinanchu/fusato/config/includes.chroot/etc/calamares
cp $uchinanchu/pepcal/install-peppermint $uchinanchu/fusato/config/includes.chroot/usr/bin
cp $uchinanchu/pepcal/sources-final $uchinanchu/fusato/config/includes.chroot/usr/sbin
cp $uchinanchu/peplightdm/lightdm.conf $uchinanchu/fusato/config/includes.chroot/etc/lightdm
cp $uchinanchu/peplightdm/lightdm-gtk-greeter.conf $uchinanchu/fusato/config/includes.chroot/etc/lightdm
cp $uchinanchu/peptools/Welcome_auto.desktop $uchinanchu/fusato/config/includes.chroot/etc/skel/.config/autostart

cp $uchinanchu/pepapplication/*  $uchinanchu/fusato/config/includes.chroot/usr/share/applications
cp $uchinanchu/pepdeffileman/xfce4/* $uchinanchu/fusato/config/includes.chroot/etc/xdg/xfce4
cp $uchinanchu/pepdeffileman/helpers/* $uchinanchu/fusato/config/includes.chroot/usr/share/xfce4/helpers
cp $uchinanchu/pepfont/* $uchinanchu/fusato/config/includes.chroot/usr/share/fonts/pepconfig
cp $uchinanchu/pepgrub/* $uchinanchu/fusato/config/includes.chroot/etc/default
cp $uchinanchu/pephooks/* $uchinanchu/fusato/config/hooks/normal
cp $uchinanchu/pepice/* $uchinanchu/fusato/config/includes.chroot/opt/iceinst
cp $uchinanchu/pepissue/* $uchinanchu/fusato/config/includes.bootstrap/etc
cp $uchinanchu/pepissue/* $uchinanchu/fusato/config/includes.chroot/etc
cp $uchinanchu/pepissue/* $uchinanchu/fusato/config/includes.chroot/opt/pepconf
cp $uchinanchu/peposrelease/* $uchinanchu/fusato/config/includes.chroot/opt/pepconf
cp $uchinanchu/peposrelease/* $uchinanchu/fusato/config/includes.chroot/usr/lib
cp $uchinanchu/peppinunstable/* $uchinanchu/fusato/config/includes.chroot/etc/apt/preferences.d
cp $uchinanchu/peppixmaps/* $uchinanchu/fusato/config/includes.chroot/usr/share/pixmaps
cp $uchinanchu/peppolkit/* $uchinanchu/fusato/config/includes.chroot/usr/share/polkit-1/actions
cp $uchinanchu/pepsources/* $uchinanchu/fusato/config/includes.bootstrap/etc/apt
cp $uchinanchu/pepsources/* $uchinanchu/fusato/config/includes.chroot/etc/apt
cp $uchinanchu/pepsources/* $uchinanchu/fusato/config/includes.chroot/opt
cp $uchinanchu/pepstartpage/* $uchinanchu/fusato/config/includes.chroot/opt/startpep
cp $uchinanchu/peptools/* $uchinanchu/fusato/config/includes.chroot/opt/pypep
cp $uchinanchu/pepuserconfig/* $uchinanchu/fusato/config/includes.chroot/etc/live/config.conf.d
cp $uchinanchu/pepwallpaper/* $uchinanchu/fusato/config/includes.chroot/usr/share/backgrounds


cp $uchinanchu/pepaliases/bash_aliases  $uchinanchu/fusato/config/includes.chroot/etc/skel/.bash_aliases


# Copy recursive files and sub-directories 
cp -dr $uchinanchu/pepicons/Obsidian $uchinanchu/fusato/config/includes.chroot/usr/share/icons
cp -dr $uchinanchu/pepicons/Obsidian-Red $uchinanchu/fusato/config/includes.chroot/usr/share/icons
cp -dr $uchinanchu/pepicons/Peppermint-10-Red $uchinanchu/fusato/config/includes.chroot/usr/share/icons
cp -dr $uchinanchu/pepicons/Peppermint-10-Red-Dark $uchinanchu/fusato/config/includes.chroot/usr/share/icons
cp -dr $uchinanchu/peptheme/Arc-Dark $uchinanchu/fusato/config/includes.chroot/usr/share/themes
cp -dr $uchinanchu/peptheme/Arc-Red-Dark $uchinanchu/fusato/config/includes.chroot/usr/share/themes
cp -dr $uchinanchu/peptheme/Peppermint-10-Red-Dark $uchinanchu/fusato/config/includes.chroot/usr/share/themes

cp -r $uchinanchu/pepcal/calamares/branding $uchinanchu/fusato/config/includes.chroot/etc/calamares
cp -r $uchinanchu/pepcal/calamares/modules $uchinanchu/fusato/config/includes.chroot/etc/calamares
cp -r $uchinanchu/pepdesktopbase/desktop-base $uchinanchu/fusato/config/includes.chroot/usr/share/
cp -r $uchinanchu/peploadersplash/boot  $uchinanchu/fusato/config/includes.binary
cp -r $uchinanchu/peploadersplash/isolinux  $uchinanchu/fusato/config/includes.binary
cp -r $uchinanchu/pepmenu/menus $uchinanchu/fusato/config/includes.chroot/etc/skel/.config
cp -r $uchinanchu/pepnemo/nemo $uchinanchu/fusato/config/includes.chroot/etc/skel/.config
cp -r $uchinanchu/pepplymouth/plymouth $uchinanchu/fusato/config/includes.chroot/usr/share/
cp -r $uchinanchu/pepxfce/xfce4 $uchinanchu/fusato/config/includes.chroot/etc/skel/.config

cp -r $uchinanchu/peploadersplash/testing/boot  $uchinanchu/fusato/config/includes.binary
cp -r $uchinanchu/peploadersplash/testing/isolinux  $uchinanchu/fusato/config/includes.binary


# Build the ISO #
lb build  #--debug --verbose 

