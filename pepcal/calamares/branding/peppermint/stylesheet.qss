/* A branding component can ship a stylesheet (like this one)
which is applied to parts of the Calamares user-interface.
In principle, all parts can be styled through CSS.
Missing parts should be filed as issues.

The IDs are based on the object names in the C++ code.
You can use the Debug Dialog to find out object names:
  - Open the debug dialog
  - Choose tab *Tools*
  - Click *Widget Tree* button
The list of object names is printed in the log.

Documentation for styling Qt Widgets through a stylesheet
can be found at
    https://doc.qt.io/qt-5/stylesheet-examples.html
    https://doc.qt.io/qt-5/stylesheet-reference.html
In Calamares, styling widget classes is supported (e.g.
using `QComboBox` as a selector).

This example stylesheet has all the actual styling commented out.
The examples are not exhaustive. */

/*########Current Calamres Styles##########*/
/* These "Q's" are master styles for overall colours */

/* Set the entire Font and Background color */
QWidget { color: #FFFFFF; background-color: #333333 }

/* Set Drop Down Font and Background color */
QComboBox { color: #FFFFFF ;
	font: 12px ;
	border-radius: 10px ;
	background-color: #454545 }

/* Set the inline Text box edit Font and Background color */
QLineEdit { color: #000000 ;
	font: 14px ;
	background-color: beige }

/* Set List box Font and Background color */
QListView { color: #FFFFFF ;
	font: 12px ;
	border-radius: 4px ;
	background-color: #454545 ;
	alternate-background-color: #454545 }

/* Set the TreeView Font and Background color */
QTreeView { color: #FFFFFF ;
	border-radius: 12px ;
	background-color: #454545 }

/* Set buttons Font and Background color */
QPushButton { color: #FFFFFF ;
	font: 14px ;
	border-color: #000000 ;
	background-color: #454545 }

/* Set the CheckBox size and Background color */
QCheckBox { color: #000000 ;
	border-width: 2px ;
	border-color: #000000 ;
	background-color: #545454 }

/* Set the color when selected */ 
QRadioButton { color: #FFFFFF }

/* Set the tip Font and Background color */
QToolTip { color: #FFFFFF ;
	font: 12px ;
	background-color: #545454 }

/* Set the Progress Bar alignment and load color */
QProgressBar { text-align: center }
QProgressBar::chunk { background-color: #770a0a }

/* These are for specific controls in specific areas of the installer*/
#tabWidget {background-color: #333333;}
#mainApp {background-color: #333333;}
#summaryStep {background-color: #333333;}
#Page_Keyboard {background-color: #333333;}
#debugButton  {font : 11px;}
#debugButton:hober {color : #000000;}

#sidebarMenuApp{ background-color: #333333;}
#aboutButton{ background-color: #333333; color: white; }
#donateButton{ background-color: #333333; color: #333333; }
#supportButton{ background-color: #333333; color: #333333; }
#knownIssuesButton{ background-color: #333333; color: #333333; }
#releaseNotesButton{ background-color: #333333; color: #333333; }
# canary yellow - #ced174 }
