#!/bin/bash


### ## # Set build working variables HERE # ## ###

CANDIDATE=release	# Sets which pepbld.sh to use and the location in /var/www/html/[release|rc|testing|nightly|unstable]
PREFIX=PepOS		# Sets a unique final name of the ISO and checksum so <HouseKeeping> only removes 2 files .
SUFFIX=amd64		# Also used by <HouseKeeping> And to distinguishe between amd64 and x86 or devuan and ubuntu . 

### ## # Make NO Edits Below This Line !! # ## ###


[ $TODAY ! = " " ] || TODAY=$(date -u +"%m-%d-%y")	# If MasterBuilder.sh is used IT will set the date. If not used, we set it here.
FileName=${PREFIX}-${TODAY}-${SUFFIX}		# This will give a uniquely named and dated ISO and checksum for <HouseKeeping>. 
LOCATION=/var/www/html/${CANDIDATE}		# Tells <HouseKeeping> and the script which 2 files to remove and where to put them. 
LogDir=/var/log/Live-Build			# This folder contains a log for the last $[PREFIX]-$[SUFFIX] build. 
WorkingDir=/root/pep_builder/PepDistroConfigs	# * If we change servers or locations T*H*I*S line is the O*N*L*Y line to change. *


# Move into the working directory.
cd ${WorkingDir}

# Make sure the local repo is up to date.
git pull
sync

# Run the build script - expect 50 minutes, allow 60.
./PepBld-${CANDIDATE}.sh 2>&1 | tee /tmp/Live-Build.log.out 


### <HouseKeeping>
# If we start keeping old checksums, check if [ $CANDIDATE = release ] and move it instead of removing it.
# Remove the previous files in ${LOCATION} .
rm -f ${LOCATION}/${PREFIX}-*-${SUFFIX}.iso
rm -f ${LOCATION}/${PREFIX}-*-${SUFFIX}-sha512.checksum
### </HouseKeeping>


# Move into the directory containing the ISO.
until [ -e fusato/*.iso ] ; do sleep 2 ; done && sync || sync
cd fusato
mv  *.iso ${FileName}.iso

# Make the checksum file.
sha512sum ${FileName}.iso > ${FileName}-sha512.checksum

# Move the ISO & checksum files.
#mv $(FileName}*  ${LOCATION}/
mv ${FileName}.iso             ${LOCATION}/${FileName}.iso
mv ${FileName}-sha512.checksum ${LOCATION}/${FileName}-sha512.checksum
touch ${LOCATION}/${FileName}*

# This only keeps the last log for each build. Getting logrotate involved isn't required.
# Is ${LogDir} a dir, if not - remove it, if removed - make dir, if nothing was there - make dir. 
[ ! -d ${LogDir} ] && rm -f ${LogDir} && mkdir -p ${LogDir} || mkdir -p ${LogDir}
mv /tmp/Live-Build.log.out ${LogDir}/${PREFIX}-${SUFFIX}.log

# Set our working directory back to the location of MasterBuilder.sh
cd ../ 

# This will "flush" our variables without handing any back to MasterBuilder.sh . 
# exit	# But NOT `return`. 

