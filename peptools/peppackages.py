import os
import subprocess
import tkinter as tk
import time
from tkinter import *
from tkinter import ttk
from tkinter import font
from ttkthemes import ThemedTk
from tkinter import PhotoImage
import apt

#list of apps for future use
checkboxstates = []
cache = apt.Cache()

unstableRepo =['firefox']

#this will set the window name and  style.
instw = ThemedTk(theme ='clam')
#the window size
instw.geometry('680x520')
#the title of the window
instw.title("Peppermint Recommendations")
#set the icon for the  window
instw.tk.call('wm', 'iconphoto', instw._w, tk.PhotoImage(file='/usr/share/pixmaps/peppermint-old.png'))
instw['bg']='#181c16'

#logo for the main window
peplogo = PhotoImage(file = "/usr/share/pixmaps/peppermint-48.png")
logo = tk.Label(instw, image=peplogo, borderwidth=0,bg='#181c16').place(x=85, y=5)

#row for all the items but the logo and the title
row = 20
row2 = 360

#main title style
spmt = ttk.Style()
spmt.configure("Red.TLabel", foreground ="#ffffff", background ="#181c16", font = ("Helvetica", '22', 'bold') ) # red was #b12026

#subtitle style
subtitlestyle = ttk.Style()
subtitlestyle.configure("Red2.TLabel", foreground ="red", background ="#181c16", font = ("Helvetica", '10') )

#styles for the lables
lstmt = ttk.Style()
lstmt.configure("White.TLabel", foreground ="#ffffff", background ="#181c16", font = ("Helvetica", '10', 'bold') )

#styles for the lables
lstinstalled = ttk.Style()
lstinstalled.configure("Dark.TLabel", foreground ="#ffffff", background ="#181c16", font = ("Helvetica", '10') )

#style for checkboxes
cbstyles = ttk.Style()
cbstyles.configure('checkbox.TCheckbutton', foreground ="#ffffff", background ="#181c16" )
cbstyles.map("TCheckbutton",background=[('pressed','#181c16'),('active','#181c16')],foreground=[('disabled','#83572b')])

###This is all the lables and verbiage used and thier placements
lblpmtitle = ttk.Label(instw, style="Red.TLabel", text="Select Your Software Packages").place(x=140, y=10)
lblpmsubtitle = ttk.Label(instw, style="Dark.TLabel", text="Darkened items are already installed. \nPackages are from Debian Repositories.").place(x=row2, y=310)
lblpmsubtitle = ttk.Label(instw, style="Red2.TLabel", text="At PeppermintOS, we let our users decide what software is on their system.").place(x=140, y=40)

lblsoftware = ttk.Label(instw, style="White.TLabel", text="Software").place(x=100, y=80)
lblbrowsers = ttk.Label(instw, style="White.TLabel", text="Browsers").place(x=440, y=80)

#installed message

#instaledtitle = ttk.Label(instw, style="Dark.TLabel", text="Colored Packages are installed.").place(x=row2, y=300)

#output textbox
textbox = Text(instw, background="#181c16",foreground="white", height="50",state='disabled')
textbox.place(x=0, y=420, height=100, width=680)
textbox.configure(font = ("GNU Unifont", '8') )

#if this package is already installed, grey out the item
def InstallCheck(appname, checkbox):
	if cache[appname].is_installed:
		checkbox.configure(state=["disabled"])

#firefox
firefox = StringVar()
firefox.set("off")
cbfirefox = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=firefox,  onvalue='firefox', offvalue="off", text="Firefox: Mozilla's Open-source browser")
cbfirefox.place(x=row2,y=100)
checkboxstates.append(firefox)
InstallCheck("firefox", cbfirefox)

#konqueror
konqueror = StringVar()
konqueror.set("off")
cbkonqueror = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=konqueror,  onvalue="konqueror", offvalue="off", text="Konqueror: KDE browser and file manager")
cbkonqueror.place(x=row2,y=120)
checkboxstates.append(konqueror)
InstallCheck("konqueror", cbkonqueror)

#epiphany-browser
epiphany = StringVar()
epiphany.set("off")
cbepiphany = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=epiphany,  onvalue="epiphany-browser", offvalue="off", text="Gnome Web (Epiphany): Intuitive browser")
cbepiphany.place(x=row2,y=140)
checkboxstates.append(epiphany)
InstallCheck("epiphany-browser", cbepiphany)

#falkon
falkon = StringVar()
falkon.set("off")
cbfalkon = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=falkon,  onvalue="falkon", offvalue="off", text="Falkon: Qt5 browser by KDE")
cbfalkon.place(x=row2,y=160)
checkboxstates.append(falkon)
InstallCheck("falkon", cbfalkon)

#torbrowser-launcher
torbrowser = StringVar()
torbrowser.set("off")
cbtorbrowser = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=torbrowser,  onvalue="torbrowser-launcher", offvalue="off", text="Tor Browser: Privacy browser")
cbtorbrowser.place(x=row2,y=180)
checkboxstates.append(torbrowser)
InstallCheck("torbrowser-launcher", cbtorbrowser)

#midori
midori = StringVar()
midori.set("off")
cbmidori = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=midori,  onvalue="midori", offvalue="off", text="Midori: Fast, lightweight browser")
cbmidori.place(x=row2,y=200)
checkboxstates.append(midori)
InstallCheck("midori", cbmidori)

#qutebrowser
qutebrowser = StringVar()
qutebrowser.set("off")
cbqutebrowser = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=qutebrowser,  onvalue="qutebrowser", offvalue="off", text="Qutebrowser: Vim-like browser")
cbqutebrowser.place(x=row2,y=220)
checkboxstates.append(qutebrowser)
InstallCheck("qutebrowser", cbqutebrowser)

#luakit
luakit = StringVar()
luakit.set("off")
cbluakit = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=luakit,  onvalue="luakit", offvalue="off", text="Luakit: Browser extensible by Lua")
cbluakit.place(x=row2,y=240)
checkboxstates.append(luakit)
InstallCheck("luakit", cbluakit)

#chromium
chromium = StringVar()
chromium.set("off")
cbchromium = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=chromium,  onvalue="chromium", offvalue="off", text="Chromium: Google's Open-Source browser")
cbchromium.place(x=row2,y=260)
checkboxstates.append(chromium)
InstallCheck("chromium", cbchromium)

#atril
atril = StringVar()
atril.set("off")
cbatril = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=atril,  onvalue="atril", offvalue="off", text="Atril: a multi-page document viewer")
cbatril.place(x=row,y=100)
checkboxstates.append(atril)
InstallCheck("atril", cbatril)

#transmission
transmission = StringVar()
transmission.set("off")
cbtransmission = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=transmission,  onvalue="transmission", offvalue="off", text="Transmission BitTorrent Client")
cbtransmission.place(x=row,y=120)
checkboxstates.append(transmission)
InstallCheck("transmission", cbtransmission)

#ARandR
arandr = StringVar()
arandr.set("off")
cbarandr = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=arandr,  onvalue="arandr", offvalue="off", text="ARandR: Another XRandR GUI")
cbarandr.place(x=row,y=140)
checkboxstates.append(arandr)
InstallCheck("arandr", cbarandr)

#pmount
pmount = StringVar()
pmount.set("off")
cbpmount = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=pmount,  onvalue="pmount", offvalue="off", text="pmount: Standard mount program")
cbpmount.place(x=row,y=160)
checkboxstates.append(pmount)
InstallCheck("pmount", cbpmount)

#dconf-editor
dconfeditor = StringVar()
dconfeditor.set("off")
cbdconfeditor = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=dconfeditor,  onvalue="dconf-editor", offvalue="off", text="DConf Editor: For environment settings")
cbdconfeditor.place(x=row,y=180)
checkboxstates.append(dconfeditor)
InstallCheck("dconf-editor", cbdconfeditor)

#gpicview
gpicview = StringVar()
gpicview.set("off")
cbgpicview = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=gpicview,  onvalue="gpicview", offvalue="off", text="GpicView image viewer")
cbgpicview.place(x=row,y=200)
checkboxstates.append(gpicview)
InstallCheck("gpicview", cbgpicview)

#parole
parole = StringVar()
parole.set("off")
cbparole = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=parole,  onvalue="parole", offvalue="off", text="Parole: Media player for Xfce")
cbparole.place(x=row,y=220)
checkboxstates.append(parole)
InstallCheck("parole", cbparole)

#mate-calc
matecalc = StringVar()
matecalc.set("off")
cbmatecalc = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=matecalc,  onvalue="mate-calc", offvalue="off", text="Mate Calculator")
cbmatecalc.place(x=row,y=240)
checkboxstates.append(matecalc)
InstallCheck("mate-calc", cbmatecalc)

#snapd
snapd = StringVar()
snapd.set("off")
cbsnapd = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=snapd,  onvalue="snapd", offvalue="off", text="Snap Package Platform")
cbsnapd.place(x=row,y=260)
checkboxstates.append(snapd)
InstallCheck("snapd", cbsnapd)

#flatpak
flatpak = StringVar()
flatpak.set("off")
cbflatpak = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=flatpak,  onvalue="flatpak", offvalue="off", text="Flatpak Packages Platform")
cbflatpak.place(x=row,y=280)
checkboxstates.append(flatpak)
InstallCheck("flatpak", cbflatpak)

#gnome-software
gnomesoftware = StringVar()
gnomesoftware.set("off")
cbgnomesoftware = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=gnomesoftware,  onvalue="gnome-software", offvalue="off", text="Gnome software store")
cbgnomesoftware.place(x=row,y=300)
checkboxstates.append(gnomesoftware)
InstallCheck("gnome-software", cbgnomesoftware)

#youtube-dl
youtubedl = StringVar()
youtubedl.set("off")
cbyoutubedl = ttk.Checkbutton(instw, style="checkbox.TCheckbutton", variable=youtubedl,  onvalue="youtube-dl", offvalue="off", text="YouTube-DL: YouTube Downloader Tool")
cbyoutubedl.place(x=row,y=320)
checkboxstates.append(youtubedl)
InstallCheck("youtube-dl", cbyoutubedl)

def install(applist, unstableapplist):
	btn.place_forget()
	textbox.configure(state='normal')

	def textout(line):
		textbox.insert(tk.END, line)
		textbox.update_idletasks()
		textbox.see("end")
		print(line + "\n")

	if(len(applist) >  0):
		with subprocess.Popen( ["apt-get","install","-y"] + applist,shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True) as p:
			if p.stdout:
				for line in p.stdout:
					textout(line)
			if p.stderr:
				for line in p.stderr:
					textout(line)
	if(len(unstableapplist) >  0):
		with subprocess.Popen( ["apt-get","install","-y","-t","unstable"] + unstableapplist,shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True) as p:
			if p.stdout:
				for line in p.stdout:
					textout(line)
			if p.stderr:
				for line in p.stderr:
					textout(line)
	textout("Done.")
	textbox.configure(state='disabled')
	btn.place(x=245, y=375, height= 30, width=150)

def getcheckboxes():
	appqueue = []
	unstableappqueue = []
	for item in checkboxstates:
		if item.get() != "off":
			if(item.get() in unstableRepo):
				unstableappqueue.append(item.get())
			else:
				appqueue.append(item.get())
	print(appqueue)
	print(unstableappqueue)
	if(len(appqueue) > 0 or len(unstableappqueue) > 0):
		install(appqueue, unstableappqueue)
	else:
		textbox.configure(state='normal')
		textbox.insert(tk.END, "No packages selected. Please select a package.\n")
		textbox.update_idletasks()
		textbox.see("end")
		textbox.configure(state='disabled')

#inital Begin  install button
btn = ttk.Button(instw,text="Install Selected",command=getcheckboxes)
##Placement of the Begin install button##
btn.place(x=245, y=375, height= 30, width=150)
##Start the window#
instw.mainloop()
