#!/bin/bash

# Setup the window
yad --center --width 500 --height 100 \
--title="Install Peppermint Additional Wallapers" \
--window-icon="/usr/share/pixmaps/peppermint-old.png" \
--text="Would you like to install additional Peppermint Wallpapers?" \
--button="Yes":1 \
--button="No":2 \

#Declared Variable
cm=$?

#if No if clicked close te form
[ $cm -eq 2 ] && exit 0
#if yes is clicked run this if statement
if [ $cm -eq 1 ]; then 

###########################################
#first move to the backgrounds folder, then download the wallpaper compressed file
#next extract the files, then remove the archive, next adjust the wallpaper location
#Finally Remove the empty folder
###########################################
cd /usr/share/backgrounds/
xfce4-terminal --execute sudo wget https://codeberg.org/Peppermint_OS/PepWallPaper/archive/master.tar.gz && sudo tar -xf master.tar.gz && sudo rm master.tar.gz && sudo mv /usr/share/backgrounds/pepwallpaper/* /usr/share/backgrounds/ && sudo rm -r pepwallpaper && sudo chown 0:0 /usr/share/backgrounds/* && sudo chmod -r /usr/share/backgrounds/* && echo "Peppermint Wallpaper Installed" 

fi

#message to the let the use know its completed
yad --center --width 500 --height 100 \
--title="Install Peppermint Additional Wallapers" \
--window-icon="/usr/share/pixmaps/peppermint-old.png" \
--text="The Additional WallPapers have been Installed!" \
