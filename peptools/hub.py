import actions
import tkinter as tk
import tkinter.ttk as ttk
from tkinter import font
from ttkthemes import ThemedTk
from tkinter import PhotoImage
import apt

#############
#things to do
#if an instance of pephub is already running, exit this one

cache = apt.Cache()

class PepHub(tk.Tk):
    def __init__(self):
        super().__init__()

        self.title("Peppermint Hub")
        self.style = ttk.Style(self)
        self.style.theme_use('clam')
        self.resizable(False, False)

        #styling for darker theme here
        self.style.configure('TFrame', background='#181C16')
        self.style.configure('TLabel',background='#181C16',foreground='#ffffff')
        self.style.configure('TButton',background='#181C16',bordercolor='#3b4536',lightcolor='#3b4536',darkcolor='#0A0B0B',)
        self.style.map("TButton",background=[('pressed','#1d221b'),('active','#272e24')],darkcolor=[('pressed','#0c0e0b')],lightcolor=[('pressed','#0c0e0b')])
        self.tk.call('wm', 'iconphoto', self._w, tk.PhotoImage(file='/usr/share/pixmaps/peppermint-old.png'))


        #The tabbing feature
        pnb = ttk.Notebook(self)
        pnb.pack(expand=True)

        # tabs and thier sized.
        tbuser = ttk.Frame(pnb, padding=30)
        tbnet = ttk.Frame(pnb, padding=30)
        tbsys = ttk.Frame(pnb, padding=30)
        tbsoft = ttk.Frame(pnb, padding=30)

        #the tab names
        pnb.add(tbuser, text='User & Tweaks')
        pnb.add(tbnet, text='Network & Hardware')
        pnb.add(tbsys, text='System & Software')

        # icons section - used self.name since regular assignment
        # ended with all images being garbage collected
        #User and tweaks
        self.icosm = PhotoImage(file = "/usr/share/pixmaps/applications-system.png")
        self.icodconf = PhotoImage(file = "/usr/share/pixmaps/dconf-editor.png")
        self.icomtp = PhotoImage(file = "/usr/share/pixmaps/mouse.png")
        self.icoclf = PhotoImage(file = "/usr/share/pixmaps/glade.png")
        self.icopp = PhotoImage(file = "/usr/share/pixmaps/panel-applets.png")
        self.icons = PhotoImage(file = "/usr/share/pixmaps/cs-notifications.png")
        self.icopa = PhotoImage(file = "/usr/share/pixmaps/cs-default-applications.png")
        self.icoks = PhotoImage(file = "/usr/share/pixmaps/keyboard.png")

        #Network And Hardware
        self.icont = PhotoImage(file = "/usr/share/pixmaps/network-wired.png")
        self.icoptr = PhotoImage(file = "/usr/share/pixmaps/gnome-dev-printer.png")
        self.icomon = PhotoImage(file = "/usr/share/pixmaps/video-display.png")
        self.icopwm = PhotoImage(file = "/usr/share/pixmaps/battery.png")
        self.icopav = PhotoImage(file = "/usr/share/pixmaps/stock_music-library.png")

        #System And Software
        #System
        self.icoug = PhotoImage(file = "/usr/share/pixmaps/stock_people.png")
        self.icodu = PhotoImage(file = "/usr/share/pixmaps/drive-harddisk-system.png")
        self.icosd = PhotoImage(file = "/usr/share/icons/Obsidian/emblems/48/emblem-marketing.png")
        self.icoum = PhotoImage(file = "/usr/share/pixmaps/update-manager.png")
        self.icoas = PhotoImage(file = "/usr/share/pixmaps/preferences-desktop-accessibility.png")
        self.icosi = PhotoImage(file = "/usr/share/pixmaps/gnome-session.png")

        #Software
        self.icoflat = PhotoImage(file = "/usr/share/pixmaps/flat.png")
        self.icosnap = PhotoImage(file = "/usr/share/pixmaps/snap.png")
        self.icoaih = PhotoImage(file = "/usr/share/pixmaps/ai.png")
        self.icospm = PhotoImage(file = "/usr/share/pixmaps/synaptic.png")
        self.icogno = PhotoImage(file = "/usr/share/pixmaps/gnome-software.png")

        #Install Icons
        self.icocr = PhotoImage(file = "/usr/share/pixmaps/peppermint-inst-48.png")

        #Button Section - Use this area to manage and configure the buttone on each tab.

        #User and Tweaks buttons
        btnuag = ttk.Button(tbuser, text='Users & Groups', width = '20',command=actions.ssug , image = self.icoug, cursor="hand2")
        btnsm = ttk.Button(tbuser, text='Settings Manager', width = '20', command=actions.osettingsmgr, image = self.icosm,cursor="hand2")
        btnas = ttk.Button(tbuser, text='Accessibilitity Settings', width = '20', command=actions.ssas, image = self.icoas, cursor="hand2")
        btdconf = ttk.Button(tbuser, text='dconf Editor',  width = '20', command=actions.odeconf, image = self.icodconf,cursor="hand2")
        btnclf = ttk.Button(tbuser, text='Customize Look and Feel',  width = '20', command=actions.oclf, image = self.icoclf,cursor="hand2")
        btnpp = ttk.Button(tbuser, text='Panel Preferences', width = '20', command=actions.opp, image = self.icopp,cursor="hand2")
        btnns = ttk.Button(tbuser, text='Notification Settings', width = '20', command=actions.ons, image = self.icons,cursor="hand2")
        btnpa = ttk.Button(tbuser, text='Preferred Applications', width = '20', command=actions.opa, image = self.icopa,cursor="hand2")

        #Network and Hardward tab buttons
        btnnm = ttk.Button(tbnet, text='Network Manager', width = '20', command=actions.nhnm, image = self.icont,cursor="hand2")
        btnprint = ttk.Button(tbnet, text='Printers', width = '20',command=actions.nhpr, image = self.icoptr, cursor="hand2")
        btndis = ttk.Button(tbnet, text='Display', width = '20',command=actions.nhdis, image = self.icomon,cursor="hand2")
        btnpmgr = ttk.Button(tbnet, text='Power Manager', width = '20',command=actions.nhpom, image = self.icopwm,cursor="hand2")
        btnvol = ttk.Button(tbnet, text='Pulse Audio Volume', width = '20',command=actions.nhpav, image = self.icopav, cursor="hand2")
        btmtp = ttk.Button(tbnet, text='Mouse and Touchpad', width = '20', command=actions.omt, image = self.icomtp,cursor="hand2")
        btnks = ttk.Button(tbnet, text='Keyboard', width = '20', command=actions.oks, image = self.icoks,cursor="hand2")
        btndu = ttk.Button(tbnet, text='Disk Utilities', width = '20',command = actions.ssdu, image = self.icodu, cursor="hand2")

        #System and Software Buttons
        btnupmgr = ttk.Button(tbsys, text='Update Manager', width = '20', image = self.icoum, command=actions.ssum, cursor="hand2")
        btnsi = ttk.Button(tbsys, text='System Information', width = '20',command=actions.sssi, image = self.icosi, cursor="hand2")
        btnflt = ttk.Button(tbsys, text='Flathub', width = '20',command=actions.ssfh, image = self.icoflat, cursor="hand2")
        btnsnap = ttk.Button(tbsys, text='Snap Store', width = '20' , command = actions.ssss, image = self.icosnap, cursor="hand2")
        btnai = ttk.Button(tbsys, text='App Image Hub', width = '20', command = actions.ssai, image = self.icoaih, cursor="hand2")
        btnspm = ttk.Button(tbsys, text='Synaptic', width = '20',command=actions.ssspm, image = self.icospm, cursor="hand2")
        btncore = ttk.Button(tbsys, text='Packages', command=actions.packages, image = self.icocr, cursor="hand2")
        btngnomesoftware = ttk.Button(tbsys, text='Gnome Software', command=actions.ssgn, image = self.icogno, cursor="hand2")
        btngnomeweb = ttk.Button(tbsys, text='Gnome Software', command=actions.ssgns, image = self.icogno, cursor="hand2")


        #these are  the label section - Use this area to manage the lables for the tabs
        #User Preferences
        lbluag = ttk.Label(tbuser, text='Users & Groups')
        lblas = ttk.Label(tbuser, text='Accessibilitity')
        lblsm = ttk.Label(tbuser, text='Settings')
        lblclf = ttk.Label(tbuser, text='Appearance')
        lblpp = ttk.Label(tbuser, text='Panel')
        lblns = ttk.Label(tbuser, text='Notification Settings')
        lblpa = ttk.Label(tbuser, text='Default Applications')
        lbldconf = ttk.Label(tbuser, text='dconf Editor')

        #Network and Hardware Labels
        lblnm = ttk.Label(tbnet, text='Network Connections')
        lblprint = ttk.Label(tbnet, text='Printers')
        lbldu = ttk.Label(tbnet, text='Disk Utilities')
        lbldis = ttk.Label(tbnet, text='Display')
        lblpmgr = ttk.Label(tbnet, text='Power Manager')
        lblvol = ttk.Label(tbnet, text='Pulse Audio Volume')
        lblmtp = ttk.Label(tbnet, text='Mouse and Touchpad')
        lblks = ttk.Label(tbnet, text='Keyboard')

        #Software Labels
        lblcore = ttk.Label(tbsys, text='Select Packages')
        lblupmgr = ttk.Label(tbsys, text='Update Manager')
        lblsi = ttk.Label(tbsys, text='System Information')
        lblsnap = ttk.Label(tbsys, text='Snap Store')
        lblai = ttk.Label(tbsys, text='App Image Hub')
        lblspm = ttk.Label(tbsys, text='Synaptic')
        lblflt = ttk.Label(tbsys, text='Flathub')
        lblgnome = ttk.Label(tbsys, text='Gnome Store')

        #Network and hardware buttons
        btnnm.grid(row=0, column=0,ipadx=2, ipady=2, padx=2, pady=2)
        btnprint.grid(row=0, column=1,ipadx=2, ipady=2, padx=2, pady=2)
        btndu.grid(row=0, column=2,ipadx=2, ipady=2, padx=2, pady=2)
        btndis.grid(row=2, column=0,ipadx=2, ipady=2, padx=2, pady=2)
        btnpmgr.grid(row=2, column=1,ipadx=2, ipady=2, padx=2, pady=2)
        btnvol.grid(row=2, column=2,ipadx=2, ipady=2, padx=2, pady=2)
        btmtp.grid(row=4, column=0,ipadx=2, ipady=2, padx=2, pady=2)
        btnks.grid(row=4, column=1,ipadx=2, ipady=2, padx=2, pady=2)

        #Network label positioning
        lblnm.grid(row=1, column=0, ipadx=2, ipady=2, padx=2, pady=2)
        lblprint.grid(row=1, column=1, ipadx=2, ipady=2, padx=2, pady=2)
        lbldu.grid(row=1, column=2, ipadx=2, ipady=2, padx=2, pady=2)
        lbldis.grid(row=3, column=0, ipadx=2, ipady=2, padx=2, pady=2)
        lblpmgr.grid(row=3, column=1, ipadx=2, ipady=2, padx=2, pady=2)
        lblvol.grid(row=3, column=2, ipadx=2, ipady=2, padx=2, pady=2)
        lblmtp.grid(row=5, column=0,ipadx=2, ipady=2, padx=2, pady=2)
        lblks.grid(row=5, column=1,ipadx=2, ipady=2, padx=2, pady=2)



        ##User and tweaks
        btnuag.grid(row=0, column=0,ipadx=2, ipady=2, padx=2, pady=2)
        btnas.grid(row=0, column=1,ipadx=2, ipady=2, padx=2, pady=2)
        btnsm.grid(row=0, column=2,ipadx=2, ipady=2, padx=2, pady=2)
        btnclf.grid(row=2, column=0,ipadx=2, ipady=2, padx=2, pady=2)
        btnpp.grid(row=2, column=1,ipadx=2, ipady=2, padx=2, pady=2)
        btnns.grid(row=2, column=2,ipadx=2, ipady=2, padx=2, pady=2)
        btnpa.grid(row=4, column=0,ipadx=2, ipady=2, padx=2, pady=2)

        #Label POS
        ##User and Tweaks
        lbluag.grid(row=1, column=0, ipadx=2, ipady=2, padx=2, pady=2)
        lblas.grid(row=1, column=1, ipadx=2, ipady=2, padx=2, pady=2)
        lblsm.grid(row=1, column=2,ipadx=2, ipady=2, padx=2, pady=2)
        lblclf.grid(row=3, column=0,ipadx=2, ipady=2, padx=2, pady=2)
        lblpp.grid(row=3, column=1,ipadx=2, ipady=2, padx=2, pady=2)
        lblns.grid(row=3, column=2,ipadx=2, ipady=2, padx=2, pady=2)
        lblpa.grid(row=5, column=0,ipadx=2, ipady=2, padx=2, pady=2)

        #Software Buttons
        #btnsad.grid(row=1, column=2,ipadx=2, ipady=2, padx=2, pady=2)
        btncore.grid(row=0, column=0,ipadx=2, ipady=2, padx=2, pady=2)

        btnupmgr.grid(row=0, column=2,ipadx=2, ipady=2, padx=2, pady=2)
        btnsi.grid(row=0, column=1,ipadx=2, ipady=2, padx=2, pady=2)
        btnflt.grid(row=2, column=1,ipadx=2, ipady=2, padx=2, pady=2)

        btnai.grid(row=4, column=0,ipadx=2, ipady=2, padx=2, pady=2)

        ##Sofwtare
        lblcore.grid(row=1, column=0, ipadx=2, ipady=2, padx=2, pady=2)
        lblsi.grid(row=1, column=1, ipadx=2, ipady=2, padx=2, pady=2)
        lblupmgr.grid(row=1, column=2, ipadx=2, ipady=2, padx=2, pady=2)

        lblflt.grid(row=3, column=1, ipadx=2, ipady=2, padx=2, pady=2)

        lblai.grid(row=5, column=0, ipadx=2, ipady=2, padx=2, pady=2)

        #conditional stuff
        # dconf, snap, and gnome-software

        #hide dconf if not installed
        if cache['dconf-editor'].is_installed:
            lbldconf.grid(row=5, column=1,ipadx=2, ipady=2, padx=2, pady=2)
            btdconf.grid(row=4, column=1,ipadx=2, ipady=2, padx=2, pady=2)

        #if snap is installed, open app, otherwise make a button to the snap store
        if cache['snapd'].is_installed:
            btnspm.grid(row=2, column=0,ipadx=2, ipady=2, padx=2, pady=2)
            lblspm.grid(row=3, column=0, ipadx=2, ipady=2, padx=2, pady=2)
        else:
            btnsnap.grid(row=2, column=0,ipadx=2, ipady=2, padx=2, pady=2)
            lblsnap.grid(row=3, column=0, ipadx=2, ipady=2, padx=2, pady=2)

        #if gnome store is installed, link the applicaton, otherwise link to their website
        if cache['gnome-software'].is_installed:
            btngnomesoftware.grid(row=2, column=2,ipadx=2, ipady=2, padx=2, pady=2)
            lblgnome.grid(row=3, column=2, ipadx=2, ipady=2, padx=2, pady=2)
        else:
            btngnomeweb.grid(row=2, column=2,ipadx=2, ipady=2, padx=2, pady=2)
            lblgnome.grid(row=3, column=2, ipadx=2, ipady=2, padx=2, pady=2)



if __name__=="__main__":
    PepHub().mainloop()
