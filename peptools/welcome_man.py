#!/usr/bin/env python3
import os
import subprocess as sp
import tkinter as tk
from tkinter import *
from tkinter import ttk
from tkinter import font
from ttkthemes import ThemedTk
from tkinter import PhotoImage
from tendo import singleton



#setting up window
pwel = ThemedTk(theme='clam')
window_height = 530
window_width = 750
pwel.title('Welcome to Peppermint')
pwel.tk.call('wm', 'iconphoto', pwel._w, tk.PhotoImage(file='/usr/share/pixmaps/peppermint-old.png'))
pwel['bg']='#111111'

#keep only instance at a time
me = singleton.SingleInstance()

#logo for the splash
lpath = PhotoImage(file = '/opt/pypep/peppermint-old.png')
lg = tk.Label(pwel, image=lpath,  width=128, height=128, borderwidth=0)
lg.place(x=10, y=10)

lpath2 = PhotoImage(file = '/opt/pypep/peppermint-word-white.png')
lg2 = tk.Label(pwel, image=lpath2,  width=600, height=80, borderwidth=0)
lg2.place(x=140, y=35)



#main title style
spmt = ttk.Style()
spmt.configure("Red.TLabel", foreground ="#b12026", background ="#111111", font = ("Helvetica", '32', 'bold') )




#main information syles
subpmt = ttk.Style()
subpmt.configure("Whiteb.TLabel", foreground ="#ffffff", background ="#111111", font = ('Helvetica', '14', 'bold') )

detpmt = ttk.Style()
detpmt.configure("det.TLabel", foreground ="#ffffff", background ="#111111", font = ('Helvetica', '12'))

#style the buttons
stbnt = ttk.Style()
stbnt.configure('pep.TButton', font = ('Helvetica', '14', 'bold'))
stbnt.map('pep.TButton', 
   foreground=[('!active', 'red'),('pressed', '#111111'), ('active', 'white')],
    background=[ ('!active','#111111'),('pressed', '#111111'), ('active', '#111111')] 
      )



###Verbiage and titel Placements###

lblpmwelttl = ttk.Label(pwel, style="Whiteb.TLabel", text="Welcome to Peppermint OS")
lblpmwelttl.place(x=10, y=150)

lblinfo1 = ttk.Label(pwel, style="det.TLabel", text="Make system changes, and customizations", wraplength=500)
lblinfo1.place(x=235, y=450)

lblinfo4 = ttk.Label(pwel, style="det.TLabel",text="Select from a variety of web browsers to install", wraplength=720)
lblinfo4.place(x=235, y=270)


lblinfo5 = ttk.Label(pwel, style="det.TLabel",text="Learn how to use ICE to create and manage your SSB's", wraplength=720)
lblinfo5.place(x=235, y=390)

lblinfo10 = ttk.Label(pwel, style="det.TLabel",text="Download all the Peppermint Wallpaper", wraplength=720)
lblinfo10.place(x=235, y=330)

lblinfo8 = ttk.Label(pwel, style="det.TLabel",text="We hope you enjoy -Everything you need & nothing you don't-",  wraplength=720)
lblinfo8.place(x=10, y=180)

lblinfo9 = ttk.Label(pwel, style="det.TLabel",text="~ @PCNetSpec 1961-2020",  wraplength=720)
lblinfo9.place(x=10, y=210)
 

#Functions to open the other pythons files. or close this screen ###

def hub ():
	pwel.destroy()
	os.system('pkexec python3 /opt/pypep/hub.py')	
	
def wte ():
	os.system('python3 /opt/pypep/expect.py')
	
def mop ():
	pwel.destroy()
	
def tut ():
	pwel.destroy()
	os.system('python3 /opt/pypep/tutorial.py')
	
def pks ():
	pwel.destroy()
	os.system('pkexec python3 /opt/pypep/peppackages.py')
	
def wls ():
	os.popen ('sh /opt/pypep/walls.sh')

def center_screen():
	""" gets the coordinates of the center of the screen """
	global screen_height, screen_width, x_cordinate, y_cordinate
 
	screen_width = pwel.winfo_screenwidth()
	screen_height = pwel.winfo_screenheight()
        # Coordinates of the upper left corner of the window to make the window appear in the center
	x_cordinate = int((screen_width/2) - (window_width/2))
	y_cordinate = int((screen_height/2) - (window_height/2))
	pwel.geometry("{}x{}+{}+{}".format(window_width, window_height, x_cordinate, y_cordinate))
	
	
	
	
	
###All the buttons and Placements##
btncore = ttk.Button(text="Open Pephub", style="pep.TButton", command=hub,  width=20)
btncore.place(x=10, y=440)
bttut = ttk.Button(text='ICE Tutorial', command=tut, style="pep.TButton", width=20 )
bttut.place(x=10, y=380)
btpks = ttk.Button(text='Install a Web Browser', command=pks, style="pep.TButton", width=20 )
btpks.place(x=10, y=260)
bttut = ttk.Button(text='Peppermint WallPaper', command=wls, style="pep.TButton", width=20 )
bttut.place(x=10, y=320)

#call Center screen
center_screen()
#Run this window#
pwel.mainloop()

