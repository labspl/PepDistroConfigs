from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtWebEngineWidgets import *
import sys

class MainWindow(QMainWindow):
#set up the main window and the url address. 
    def __init__(self, *args, **kwargs):
        super(MainWindow,self).__init__(*args, **kwargs)
        self.setWindowTitle("Peppermint ICE Tutorial")
        self.browser = QWebEngineView()
        self.browser.setUrl(QUrl("https://peppermintos.com/guide/ice/"))
        self.setCentralWidget(self.browser)
        self.show()

app = QApplication(sys.argv)
window = MainWindow()

app.exec_()
