import os
import subprocess
import apt

# User and Tweaks #

#get apt Cache
cache = apt.Cache()

#Settings Manager
def osettingsmgr():
	subprocess.Popen ("xfce4-settings-manager")
	pass

#Deconf Editor
def odeconf():
	subprocess.Popen ("dconf-editor")

#Mouse Settings
def omt():
	subprocess.Popen ("xfce4-mouse-settings")

#Appearence
def oclf():
	subprocess.Popen ("xfce4-appearance-settings")

#Panel Preferences
def opp():
	subprocess.Popen (["xfce4-panel", "--preferences"])

#Notifications Configuration
def ons():
	subprocess.Popen ("xfce4-notifyd-config")

#Default  settings
def opa():
	subprocess.Popen ("xfce4-mime-settings")

#Keyboardsettings
def oks():
	subprocess.Popen ("xfce4-keyboard-settings")


# Network and Hardware #


#Network Manager
def nhnm():
	subprocess.Popen ("nm-connection-editor")

#Printers
def nhpr():
	subprocess.Popen ("system-config-printer")

#Display
def nhdis():
	subprocess.Popen ("xfce4-display-settings")

#Power Manager
def nhpom():
	subprocess.Popen ("xfce4-power-manager-settings")

#Pulse Audio Volume
def nhpav():
	subprocess.Popen ("pavucontrol")

# Settings and Software #

#User and  Groups
def ssug():
	subprocess.Popen ("users-admin")

#Disk Utilities
def ssdu():
	subprocess.Popen ("gnome-disks")

#Update Manager
def ssum():
	#subprocess.Popen(["pkexec","apt-get", "dist-upgrade","-y"])

	cmd = "xfce4-terminal -e 'bash -c \"sudo apt update && sudo apt dist-upgrade -y\"' -T \"Peppermint Update\""
	subprocess.Popen(cmd,shell=True)

#xfce4-terminal -e 'bash -c "sudo apt update && sudo apt dist-upgrade -y"' -T "Peppermint Update"
#Accessibilitity
def ssas():
	subprocess.Popen ("xfce4-accessibility-settings")

#System Informaion
def sssi():
	cmd = "xfce4-terminal -e 'bash -c \"neofetch\";bash'"
	subprocess.Popen(cmd,shell=True)

#Flathub
urlfh = "https://flathub.org/"
def ssfh():
	subprocess.Popen (["x-www-browser",urlfh])

#Snapstore
urlsd = "https://snapcraft.io/snap-store"
def ssss():
	subprocess.Popen (["x-www-browser",urlsd])

#App Image
urlai = "https://www.appimagehub.com/"
def ssai():
	subprocess.Popen (["x-www-browser",urlai])

#gnomestore
def ssgn():
	subprocess.Popen ("gnome-software")

#gnomestoreweb
urlgnomestore = "https://wiki.gnome.org/Projects/SandboxedApps"
def ssgns():
	subprocess.Popen (["x-www-browser",urlgnomestore])

#Synaptic Package Manager
def ssspm():
	subprocess.Popen ("synaptic-pkexec")

#Installer
def packages():
	subprocess.Popen(["pkexec", "python3", "/opt/pypep/peppackages.py"])
