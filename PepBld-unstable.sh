#!/bin/bash
PATH="/sbin:/usr/sbin:/usr/local/sbin:$PATH"
#exec 3>&1 4>&2
#trap 'exec 2>&4 1>&3' 0 1 2 3
#exec 1>log.out 2>&1
# Remove fusato/*
# Incase of a failed build, resulting in stale mounts, add error handling after `lb build` exits.
[ -e fusato ] && [ ! -d fusato ] && rm -rf fusato && mkdir fusato
#rm -rf  fusato/*
#rm -rf  fusato/.build

# Set the working folder variable
uchinanchu="$(pwd)"

# Move to the build folder
cd fusato

lb clean

# Set of the structure to be used for the ISO and Live system.
# See /usr/lib/live/build/config for a full list of examples.
# Up above is the manual description of what options I used so far.
lb config \
--color \
--quiet \
--archive-areas "main contrib non-free" \
--architectures amd64 \
--apt-recommends false \
--binary-images iso-hybrid \
--cache true \
--mode debian \
--distribution bullseye \
--firmware-binary true \
--firmware-chroot true \
--iso-application "PeppermintOS" \
--iso-preparer "PeppermintOS-https://peppermintos.com/" \
--iso-publisher "Peppermint OS Team" \
--iso-volume "PeppermintOS" \
--image-name "PepOS" \
--linux-flavours amd64 \
--security false \
--updates true \
--win32-loader false \
--checksums sha512 \
--zsync false \


# Install the XFCE Desktop 
mkdir -p     ./config/package-lists/
echo xfce4 > ./config/package-lists/desktop.list.chroot 

# Install software
echo "# Install software to the squashfs for calamares to unpack to the OS.
arandr
arc-theme
calamares-settings-debian
calamares
curl
cups
cups-client
dbus-x11
dconf-editor
dconf-gsettings-backend
desktop-base
dkms
dmraid
dosfstools
e2fsprogs
exo-utils
firmware-linux-nonfree
firmware-linux
firmware-misc-nonfree
firmware-realtek
firmware-atheros
firmware-bnx2
firmware-bnx2x
firmware-brcm80211
firmware-intelwimax
firmware-iwlwifi
firmware-libertas
firmware-netxen
firmware-zd1211
fonts-cantarell
gparted
gnome-disk-utility
gnome-system-tools
grub-common
gvfs-backends
gir1.2-webkit2-4.0
host
initramfs-tools
inxi
libnss-mdns
lightdm
lightdm-gtk-greeter
locales-all
menu
menulibre
mtools
nemo
neofetch
network-manager-gnome
ntfs-3g
plymouth
plymouth-label
plymouth-themes
python3-pip
python3-tk
python3-bs4
python3-requests
python3-ttkthemes
python3-pyqt5.qtsvg
python3-pyqt5.qtwebkit
python3-apt
reiserfsprogs
reiser4progs
samba
screenfetch
smartmontools
sqlite3
sudo
synaptic
system-config-printer
system-config-printer-udev
mousepad
xdg-user-dirs
xfce4
xfce4-battery-plugin
xfce4-clipman-plugin
xfce4-notifyd
xfce4-power-manager
xfce4-power-manager-plugins
xfce4-taskmanager
xfce4-terminal
xfce4-screenshooter
xfce4-whiskermenu-plugin
yad
winbind
wget
xauth
xinit

" > ./config/package-lists/packages.list.chroot 


# Get packages used ONLY by the live-session, to be stored in /pool by `lb`.
echo "# Packages required only by the live session.
" > ./config/package-lists/installer.list.binary


# Setup the build directory structures
mkdir -p ./config/hooks/live
mkdir -p ./config/hooks/normal

mkdir -p ./config/includes.binary

mkdir -p ./config/includes.bootstrap/etc

mkdir -p ./config/includes.chroot/etc/apt
mkdir -p ./config/includes.chroot/etc/apt/preferences.d
mkdir -p ./config/includes.chroot/etc/calamares
mkdir -p ./config/includes.chroot/etc/default
mkdir -p ./config/includes.chroot/etc/lightdm
mkdir -p ./config/includes.chroot/etc/live/config.conf.d
mkdir -p ./config/includes.chroot/etc/skel/.config/autostart
mkdir -p ./config/includes.chroot/etc/skel/.local/share
mkdir -p ./config/includes.chroot/etc/xdg/xfce4
mkdir -p ./config/includes.chroot/opt/iceinst
mkdir -p ./config/includes.chroot/opt/pepconf
mkdir -p ./config/includes.chroot/opt/pypep
mkdir -p ./config/includes.chroot/opt/startpep
mkdir -p ./config/includes.chroot/usr/bin
mkdir -p ./config/includes.chroot/usr/sbin
mkdir -p ./config/includes.chroot/usr/lib/peppermint/ice
mkdir -p ./config/includes.chroot/usr/share/applications
mkdir -p ./config/includes.chroot/usr/share/backgrounds
mkdir -p ./config/includes.chroot/usr/share/desktop-base
mkdir -p ./config/includes.chroot/usr/share/fonts/pepconf
mkdir -p ./config/includes.chroot/usr/share/icons/default
mkdir -p ./config/includes.chroot/usr/share/ice/locale
mkdir -p ./config/includes.chroot/usr/share/peppermint
mkdir -p ./config/includes.chroot/usr/share/pixmaps
mkdir -p ./config/includes.chroot/usr/share/plymouth
mkdir -p ./config/includes.chroot/usr/share/polkit-1/actions
mkdir -p ./config/includes.chroot/usr/share/themes
mkdir -p ./config/includes.chroot/usr/share/xfce4/helpers


# Copy Multiple files to the bootstrap
cp $uchinanchu/pepissue/* ./config/includes.bootstrap/etc/


# Recursively Copy files and sub-directories to the binary
cp -r $uchinanchu/peploadersplash/unstable/boot  ./config/includes.binary
cp -r $uchinanchu/peploadersplash/unstable/isolinux  ./config/includes.binary


# Copy Single files to the chroot
cp $uchinanchu/pepaliases/bash_aliases ./config/includes.chroot/etc/skel/.bash_aliases
cp $uchinanchu/pepcal/adddesktopicon/add-calamares-desktop-icon ./config/includes.chroot/usr/bin
cp $uchinanchu/pepcal/calamares/settings.conf ./config/includes.chroot/etc/calamares
cp $uchinanchu/pepcal/install-peppermint ./config/includes.chroot/usr/bin
cp $uchinanchu/pepcal/sources-final ./config/includes.chroot/usr/sbin
cp $uchinanchu/peplightdm/lightdm.conf ./config/includes.chroot/etc/lightdm
cp $uchinanchu/peplightdm/lightdm-gtk-greeter.conf ./config/includes.chroot/etc/lightdm
cp $uchinanchu/peptools/Welcome_auto.desktop ./config/includes.chroot/etc/skel/.config/autostart


# Copy Multiple files to the chroot
cp $uchinanchu/pepapplication/* ./config/includes.chroot/usr/share/applications
cp $uchinanchu/pepdeffileman/xfce4/* ./config/includes.chroot/etc/xdg/xfce4
cp $uchinanchu/pepdeffileman/helpers/* ./config/includes.chroot/usr/share/xfce4/helpers
cp $uchinanchu/pepfont/* ./config/includes.chroot/usr/share/fonts/pepconf
cp $uchinanchu/pepgrub/* ./config/includes.chroot/etc/default
cp $uchinanchu/pephooks/* ./config/hooks/normal
cp $uchinanchu/pepice/* ./config/includes.chroot/opt/iceinst
cp $uchinanchu/pepissue/* ./config/includes.chroot/etc
cp $uchinanchu/pepissue/* ./config/includes.chroot/opt/pepconf
cp $uchinanchu/peppixmaps/* ./config/includes.chroot/usr/share/pixmaps
cp $uchinanchu/peppinunstable/* ./config/includes.chroot/etc/apt/preferences.d
cp $uchinanchu/peposrelease/* ./config/includes.chroot/usr/lib
cp $uchinanchu/peposrelease/* ./config/includes.chroot/opt/pepconf
cp $uchinanchu/peppolkit/* ./config/includes.chroot/usr/share/polkit-1/actions
cp $uchinanchu/pepsources/* ./config/includes.chroot/etc/apt
cp $uchinanchu/pepsources/* ./config/includes.chroot/opt
cp $uchinanchu/pepstartpage/* ./config/includes.chroot/opt/startpep
cp $uchinanchu/peptools/* ./config/includes.chroot/opt/pypep
cp $uchinanchu/pepuserconfig/* ./config/includes.chroot/etc/live/config.conf.d
cp $uchinanchu/pepwallpaper/* ./config/includes.chroot/usr/share/backgrounds


# Recursively Copy files and sub-directories to the chroot
cp -dr $uchinanchu/pepicons/Obsidian ./config/includes.chroot/usr/share/icons
cp -dr $uchinanchu/pepicons/Obsidian-Red ./config/includes.chroot/usr/share/icons
cp -dr $uchinanchu/peptheme/Arc-Dark ./config/includes.chroot/usr/share/themes
cp -dr $uchinanchu/peptheme/Arc-Red-Dark ./config/includes.chroot/usr/share/themes
cp -dr $uchinanchu/peptheme/Peppermint-10-Red-Dark ./config/includes.chroot/usr/share/themes

cp -r $uchinanchu/pepcal/calamares/branding ./config/includes.chroot/etc/calamares
cp -r $uchinanchu/pepcal/calamares/modules ./config/includes.chroot/etc/calamares
cp -r $uchinanchu/pepdesktopbase/desktop-base ./config/includes.chroot/usr/share/
cp -r $uchinanchu/pepnemo/nemo ./config/includes.chroot/etc/skel/.config
cp -r $uchinanchu/pepmenu/menus ./config/includes.chroot/etc/skel/.config
cp -r $uchinanchu/pepplymouth/plymouth ./config/includes.chroot/usr/share/
cp -r $uchinanchu/pepxfce/xfce4 ./config/includes.chroot/etc/skel/.config


# Build the ISO #
lb build  #--debug --verbose 

